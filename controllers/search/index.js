'use strict';


var SearchModel = require('../../models/search');


module.exports = function (router) {

    var model = new SearchModel();


    router.get('/', function (req, res) {
        
      res.render('search/index');
        
    });

};
