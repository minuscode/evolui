'use strict';

var SectionsModel = require('../../models/sections');
var fetchSections = require('../../lib/fetchSections');
var q = require('q');

module.exports = function (router) {

    var model = new SectionsModel();
    router.use(fetchSections);
    
    router.get('/home', function (req, res) {

      req.data.sections
      .then(function(sections){ 
        res.render('index', {sections: sections.results});
      })  

    });

    router.get('/partners', function (req, res) {

      req.data.sections
      .then(function(sections){    
        res.render('static_pages/partners', {sections: sections.results});
      })  

    });
    
    router.get('/contacts', function (req, res) {

      req.data.sections
      .then(function(sections){    
        res.render('static_pages/contacts', {sections: sections.results});
      })  

    });

    router.get('/faqs', function (req, res) {

      req.data.sections
      .then(function(sections){    
        res.render('static_pages/faqs', {sections: sections.results});
      })  
    
    });

    router.get('/regulamento', function (req, res) {

      req.data.sections
      .then(function(sections){    
        res.render('static_pages/regulamento', {sections: sections.results});
      }) 

    });

};
