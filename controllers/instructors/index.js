'use strict';


var InstructorsModel = require('../../models/instructors');


module.exports = function (router) {

  var model = new InstructorsModel();


  router.get('/:id', function (req, res) {
      
    res.render('instructors/show', model);
      
  });

};
