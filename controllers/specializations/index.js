'use strict';


var SpecializationsModel = require('../../models/specializations');


module.exports = function (router) {

  var model = new SpecializationsModel();

  router.get('/', function (req, res) {

    var specializations = model.findAll();

    res.render('specializations/index', { specializations: specializations });
  });

  router.get('/:id', function (req, res) {

    var specialization = model.findById(req.params.id);

    res.render('specializations/show', { specialization: specialization });
  });

};
