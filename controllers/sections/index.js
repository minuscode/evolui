'use strict';

var fetchSections = require('../../lib/fetchSections');
var SectionsModel = require('../../models/sections');
var q = require('q');


module.exports = function (router) {

  var model = new SectionsModel();
  router.use(fetchSections);

  router.get('/:id', function (req, res) {

      req.data.sections
      .then(function(sections){                      
        return [sections, model.findById(req.params.id)]
      })
      .spread(function(sections, section) {  
        return [sections, section, model.getSectionCourses(section)];
      })
      .spread(function(sections, section, courses) {
          res.render('sections/show', { section: section, courses: courses.results, sections:sections.results });
      });

  });
};
