'use strict';


var CertificatesModel = require('../../models/certificates');


module.exports = function (router) {

    var model = new CertificatesModel();

    router.get('/new', function (req, res) {
        
      res.render('certificates/new');
  
    });

};

