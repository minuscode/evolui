'use strict';


var ForumModel = require('../../models/forum');


module.exports = function (router) {

    var model = new ForumModel();


    router.get('/', function (req, res) {
        
    res.render('forum/index');
        
    });

};
