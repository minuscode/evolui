'use strict';

module.exports = function (router) {

    router.get('/', function (req, res) {
        
        res.redirect('/static_pages/home');
        
    });

};
