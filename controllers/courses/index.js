'use strict';

var CoursesModel = require('../../models/courses');

module.exports = function (router) {

  var model = new CoursesModel();


  router.get('/', function (req, res) {

    var view = req.query.view;

    model.findAll()
    .then(function(courses){
      res.render('courses/index', { view: view, courses: courses.results });  

    }, function(error){
      console.log(error);
    });
  });

  router.get('/:id', function (req, res) {

    model.getCourseDetails(req.params.id)
    .then(function(course){

      res.render('courses/show', {course: course}); 

    }, function(error){
      console.log(error);
    }

    );

  });

};
