'use strict';


var UsersModel = require('../../models/users');


module.exports = function (router) {

    var model = new UsersModel();


    router.get('/edit', function (req, res) {
        
      res.render('users/edit');
        
    });

    router.get('/my_profile', function (req, res) {
        
      res.render('users/my_profile');
        
    });

};
