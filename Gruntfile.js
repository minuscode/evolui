'use strict';

module.exports = function (grunt) {

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-dustjs');
  grunt.loadNpmTasks('grunt-copy-to');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');

  grunt.initConfig({

    sass: {
      dist: {
        options: {
          style: 'compressed',
          sourcemap: 'none'
        },
        files: [{
          expand: true,
          cwd: 'public/css',
          src: ['main.scss'],
          dest: '.build/css',
          ext: '.css'
        }]
      }
    },

    concat: {
      js: {
        src: ['public/js/app.js', 'public/js/libs/*.js'],
        dest: 'tmp/main.js'
      }
    },

    uglify: {
      dest: {
        files: {
          'tmp/main.min.js': ['tmp/main.js']
        }
      }
    },

    dustjs: {
      build: {
        files: [
          {
            expand: true,
            cwd: 'public/templates',
            src: ['**/*.dust', '*.dust'],
            dest: '.build/templates',
            ext: '.js'
          }
        ],
      }
    },

    copyto: {
      build: {
        files: [
          { cwd: 'tmp',
            src: ['main.min.js'],
            dest: '.build/js/' }
        ]
      }
    },

    clean: ["tmp/"],

    watch: {
      js: {
        files: ['public/js/*.js'],
        tasks: ['concat', 'uglify', 'copyto', 'clean']
      },
      templates: {
        files: ['public/templates/*.dust', 'public/templates/**/*.dust'],
        tasks: ['dustjs']
      },
      css: {
        files: ['public/css/*.scss', 'public/css/**/*.scss', 'public/css/**/**/*.scss'],
        tasks: ['sass']
      }
    },

    jshint: {
      all: ['Gruntfile.js', 'lib/**/*.js', 'test/**/*.js', 'controllers/**/*.js', 'models/**/*.js', 'server.js', 'index.js']
    }
  });
  
  grunt.event.on('watch', function(action, filepath, target) {
      grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
  });

  grunt.registerTask('build', [ 'sass', 'concat', 'uglify', 'dustjs', 'copyto', 'clean']);
};
