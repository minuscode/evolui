'use strict';

var api = require('../lib/api.js'),
    q = require('q'),
    _ = require('lodash');

module.exports = function CoursesModel() {

  this.findAll = function() {

    return api.getEntities({ type: 'curso', idx: 'all', order_by: 'nome', fields: ['nome', 'id', 'preco_original', 'g_1200x678' ] });

  },

  this.findById = function(id) {

    return {
      id: 1,
      title: 'A Actividade de Transporte e a Logística',
      price: '€74,90',
      date: '18 de Janeiro',
      duration: '16',
      about: 'A Actividade de Transporte e a Logística',
      imgUrl: '/img/thumbs/gestao-comercial/transportes-thumb.jpg',
      promo: true,
      promoPrice: '€65,90'
    };
  };

  this.getCourseDetails = function(id, callback) {

    return api.getEntity('curso', id);
    
  };

};
