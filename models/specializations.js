'use strict';


module.exports = function SpecializationsModel() {
  
  this.findAll = function() {
    return [
      {
        id: 1,
        title: 'Curso Completo de Microsoft Excel',
        price: '€74,90',
        about: 'Realizado maioritariamente em' +
                'e-learning, este curso, homologado ' + 
                'pelo IEFP, permite a obtenção do' + 
                'Certificado de Competências' +
                'Pedagógicas (CCP) de formador profissional (antigo CAP).',
        imgUrl: '/img/thumbs/gestao-comercial/transportes-thumb.jpg'
      },
      {
        id: 2,
        title: 'Curso Completo de Office',
        price: '€149,90',
        about: 'Realizado maioritariamente em' +
                'e-learning, este curso, homologado ' + 
                'pelo IEFP, permite a obtenção do' + 
                'Certificado de Competências' +
                'Pedagógicas (CCP) de formador profissional (antigo CAP).',
        imgUrl: '/img/thumbs/shst-thumb.jpg'
      }
    ];
  };

  this.findById = function(id) {
    return {
      id: 1,
      title: 'Curso Completo de Microsoft Excel',
      price: '€74,90',
      about: 'Realizado maioritariamente em' +
                'e-learning, este curso, homologado ' + 
                'pelo IEFP, permite a obtenção do' + 
                'Certificado de Competências' +
                'Pedagógicas (CCP) de formador profissional (antigo CAP).',
      imgUrl: '/img/thumbs/gestao-comercial/transportes-thumb.jpg'
    };
  };
};
