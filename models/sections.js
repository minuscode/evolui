'use strict';

var api = require('../lib/api.js'),
    q = require('q'),
    _ = require('lodash');


module.exports = function SectionsModel() {
  
  this.findAll = function() {
    
    return api.getEntities({ type: 'seccao', idx: 'all', order_by: 'nome' });

  }

  this.findById = function(id){

    return api.getEntity('seccao', id);

  }

  this.getSectionCourses = function(section){

    return api.getEntities({type: 'curso', idx: ['seccao', section.id], order_by: 'nome', section: section, fields: ['nome', 'id', 'preco_original', 'g_1200x678' ]});
  
  }

};


