'use strict';

var promiseFactory = require("q").Promise,
    redis_debug = require('redis'),
    redis = require('promise-redis')(promiseFactory),
    _ = require('lodash');

// redis_debug.debug_mode = true;

// Redis connection
var redis_port = process.env.REDIS_PORT || 6379,
    redis_host = process.env.REDIS_HOST || '172.31.1.7';

var client = redis.createClient(redis_port, redis_host);
client.on("error", function(err) {
    console.log('Redis failed to connect to ' + redis_host + ':' + redis_port + ' - ' + err);
});
client.select(6).then(function() {
    console.log('Connection to ' + redis_host + ':' + redis_port + ', selected DB 6');
});


// our API
module.exports = {};
module.exports.getEntity = function getEntity(type, id, options) {
    var key = 'E:';
    if (typeof id === 'object') {
        key = 'E:' + type;
        options = id;
    } else if (typeof id === 'undefined') {
        key = 'E:' + type;
    } else {
        key = 'E:' + type + ':' + id;
        options = options || {};
    }

    return client.hgetall(key).then(function(ent) {
        ent.e = JSON.parse(ent.e);
        return ent;
    });
};

module.exports.getEntityBySlug = function getEntityBySlug(type, slug, options) {
    var key = 'SL:' + type + ':' + slug;
    return client.get(key).then(function(ent) {
        return module.exports.getEntity(ent);
    });
};

module.exports.getEntities = function getEntities(options) {
    var type = options.type;
    var idx = options.idx;
    var rel = options.related;
    var via = options.via;

    // Define our key based on the indexing requested
    if (rel && via) {
        idx = 'IDX:' + via.type + ':' + via.id + ':' + rel;
    } else if (typeof idx === 'object') {
        if (idx.length === 1) { // idx: [something] => uses <type> as class of index
            idx = ['IDX', type, idx[0]].join(':');
        } else { // idx: [class, id] => direct map
            idx = 'IDX:' + idx.join(':');
        }
    } else if (idx.indexOf(':') === -1) { // idx: "id" => uses <type> as class of index
        idx = ['IDX', type, idx].join(':');
    } else {
        idx = 'IDX:' + idx;
    }
    var args = [idx];

    // Options for orderering
    if (options.order_by) {
        args.push('BY', 'E:*->ord_' + options.order_by);
    }
    if (!options.numeric) {
        args.push('ALPHA');
    }
    if (options.direction) {
        args.push(options.direction);
    }

    // obtaining fields
    var fields = options.fields || ['id', 'nome'];
    args.push(_.map(fields, function(v) {
        return ['GET', 'E:*->' + v];
    }));

    // sort and map back to objects
    args = _.flatten(args, true);
    var res = client.sort.apply(client, args);
    return res.then(function(list) {
        var rl = [];
        while (list.length) {
            var o = _.zipObject(fields, list.splice(0, fields.length));
            rl.push(o);
        }
        return {
            results: rl,
            options: options
        };
    });
};
