var SectionsModel = require('../models/sections');

module.exports = function (req, res, next) {

  var model = new SectionsModel();

  req.data={};
  req.data.sections =  model.findAll();

  next();
}


