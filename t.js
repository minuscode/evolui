'use strict';

var api = require('./lib/api.js'),
    q = require('q'),
    _ = require('lodash');

var reqs = [
    api.getEntity('curso', 667),
    api.getEntity('curso:667'),
    api.getEntityBySlug('curso', 'gestao_competencias_interpessoais'),
    api.getEntities({
        type: 'curso',
        idx: 'all',
        order_by: 'nome',
    }),
    api.getEntityBySlug('seccao', 'internacional').then(function(s) {
        return api.getEntities({
            type: 'curso',
            idx: ['seccao', s.id],
            order_by: 'nome',
            section: s,
        });
    }),

    api.getEntityBySlug('formador', 'Rosario_Cacao').then(function(t) {
        return api.getEntities({
            type: 'curso',
            idx: ['formador', t.id, 'cursos'],
            order_by: 'nome',
            teacher: t
        });
    }),

    api.getEntityBySlug('formador', 'Rosario_Cacao').then(function(t) {
        return api.getEntities({
            type: 'curso',
            related: 'cursos',
            via: t,
            order_by: 'nome'
        });
    }),

    api.getEntity('curso', 664).then(function(c) {
        return api.getEntities({
            type: 'formador', // type ob objects you'll get
            related: 'formadores', // name of the relation
            via: c // source object, in this case a course
        });
    }),

    api.getEntity('curso', 243).then(function(c) {
        return api.getEntities({
            type: 'seccao', // type ob objects you'll get
            related: 'seccoes', // name of the relation
            via: c // source object, in this case a course
        });
    }),
];



q.allSettled(reqs).spread(function(e1, e2, e3, l1, l2, l3, l4, l5, l6) {
    console.log('===== Entity: ' + e1.value.id + ' ' + e1.value.nome);
    console.log('===== Entity: ' + e2.value.id + ' ' + e2.value.nome);
    console.log('===== Entity: ' + e3.value.id + ' ' + e3.value.nome);

    l1 = l1.value;
    console.log('===== 1: List of ' + _.flatten([l1.options.type, l1.options.idx], true).join(' ') + ': ' + l1.results.length + ' elements');

    l2 = l2.value;
    var s = l2.options.section;
    console.log('===== 2: Seccao: ' + s.id + ' ' + s.nome);
    console.log('===== List of ' + _.flatten([l2.options.type, l2.options.idx], true).join(' ') + ': ' + l2.results.length + ' elements');
    l2.results.forEach(function(e) {
        console.log('       * ' + e.id + ' - ' + e.nome);
    });

    l3 = l3.value;
    var t = l3.options.teacher;
    console.log('===== 3: Formador: ' + t.id + ' ' + t.nome);
    console.log('===== List of ' + _.flatten([l3.options.type, l3.options.idx], true).join(' ') + ': ' + l3.results.length + ' elements');

    l4 = l4.value;
    t = l4.options.via;
    console.log('===== 4: Formador: ' + t.id + ' ' + t.nome);
    console.log('===== List of related ' + l4.options.related + ': ' + l4.results.length + ' elements');

    l5 = l5.value;
    var c = l5.options.via;
    console.log('===== 5: Curso: ' + c.id + ' ' + c.nome);
    console.log('===== Formadores via relation ' + l5.options.related + ': ' + l5.results.length + ' elements');
    l5.results.forEach(function(e) {
        console.log('       * ' + e.id + ' - ' + e.nome);
    });

    l6 = l6.value;
    var c = l6.options.via;
    console.log('===== 6: Curso: ' + c.id + ' ' + c.nome);
    console.log('===== Seccoes via relation ' + l6.options.related + ': ' + l6.results.length + ' elements');
    l6.results.forEach(function(e) {
        console.log('       * ' + e.id + ' - ' + e.nome);
    });

    process.exit(0);
}).
catch (function(e) {
    console.log(e.stack);
});
